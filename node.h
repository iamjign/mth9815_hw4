#ifndef NODE_H
#define NODE_H

template<typename T>
class Node
{
    public:
        Node();
        Node(T value);
        Node(const Node<T>& other);
        virtual ~Node();


        T Getdatum() { return datum_; };
        void Setdatum(T val) { datum_ = val; };
        Node<T>* Getnext() {return next_;};
        void Setnext( Node<T>* anext){ next_ = anext;}
        int Getindex() { return index_; };
        void Setindex(T val) { index_ = val; };
    private:
        T datum_;
        int index_; //unique id for each node
        Node<T>* next_;

};




template<class T>
class ListIterator
{
    public:
        ListIterator(Node<T>* head_node);
        bool hasNext();
        T Next();

    private:
        Node<T>* current_node;
};





template <typename T>
class mypair {
    T a, b;
  public:
    mypair (T first, T second)
      {a=first; b=second;}
    T getmax ();
};

template <typename T>
T mypair<T>::getmax ()
{
  T retval;
  retval = a>b? a : b;
  return retval;
}



#endif // NODE_H
