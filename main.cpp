//Testing


#include <iostream>
#include "node.cpp" //template instantiation
#include "linkedlist.cpp"
#include "doublylinkedlist.cpp"
#include "dnode.cpp"

using namespace std;

int main()
{
    LinkedList<int> llist;
    DoublyLinkedList<int> dlist;

    for(int i = 1; i<6; ++i)
    {
        llist.Add(i);
        dlist.Addbefore(i);
       // dlist.Addafter(i);
    }




    Node <int> node1(75), node2(55);

    Node <int>* nodeptr;

    node1.Setnext(&node2);

    nodeptr = node1.Getnext();

    cout<<nodeptr->Getdatum()<<endl;

    cout<<llist.Get(3)<<endl;


    cout<<"********************"<<endl;

    DNode<int> dnode1(44), dnode2(33), dnode3(66);

    dnode1.Setprev(NULL); dnode1.Setnext(&dnode2); dnode2.Setprev(&dnode1); dnode2.Setnext(&dnode3);

    DNode<int>* dnodeptr = & dnode3;

    dnodeptr->Setnext(NULL);

    cout<<dnode2.Getdatum()<<endl;

    cout<<dlist.IndexOf(3)<<endl;


    cout<<dlist.Size()<<endl;

  //  dlist.Insert(1000, 3);

    cout<<"********************"<<endl;

    for (int i =1; i!=dlist.Size()+1; i++)
    {
        cout<<dlist.IndexOf(i)<<endl;
    }

   //     cout<<dlist.Get(4)<<endl;
        //cout<<dlist.IndexOf(5)<<endl;
    return 0;
}
