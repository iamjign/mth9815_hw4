#ifndef DNODE_H
#define DNODE_H

#include "node.h"

template <typename T>
class DNode : public Node<T>
{
    public:
        DNode();
        DNode(T value);
        DNode(const DNode<T>& other);
        virtual ~DNode();

        DNode<T>* Getprev() { return prev_; };
        DNode<T>* Getnext() {return dnext_;};
        void Setprev(DNode<T>* val) { prev_ = val; };
        void Setnext(DNode<T>* val) {dnext_ = val;};
    private:
        DNode<T>* prev_;
        DNode<T>* dnext_;
};

#endif // DNODE_H
