#include "doublylinkedlist.h"

template <typename T>
DoublyLinkedList<T>::DoublyLinkedList()
{
    //initialize head and tail to NULL address

    std::cout<<"dlink before"<<std::endl;
    head_=NULL;

    std::cout<<"NOW the tail"<<std::endl;

    tail_=NULL;
    std::cout<<"dlink initialized"<<std::endl;

}


template <typename T>
DoublyLinkedList<T>::~DoublyLinkedList()
{
    if(head_ != NULL)
    {
        while(head_->Getnext()!=NULL)
        {
            DNode<T>* tmp1 = head_;
            head_=tmp1->Getnext();
            delete tmp1;

        };

        delete head_;//dtor
    }
}

template <typename T>
DoublyLinkedList<T>::DoublyLinkedList(const DoublyLinkedList<T>& other)
{

    //copy ctor
}

template <typename T>
void DoublyLinkedList<T>::Addbefore (T value)
{

    DNode<T> * newDNode = new DNode<T>(value);
    newDNode->Setprev(NULL);//this is equivalent to head_->Setprev(NULL)

    if (head_!=NULL)
    {
        std::cout<<"check point"<<endl;

        newDNode->Setnext(head_);
        head_->Setprev(newDNode);
        head_=newDNode;

        //reindex
        DNode<T>* tmp = head_;
        size_++;
        for(int idx=1; idx< size_+1; idx++)
            {
                tmp->Setindex(idx);
                tmp = tmp->Getnext();
                //tmpidx++;
            }


    }
    else
    {
        head_=newDNode;

        std::cout<<"newnode inserted to head"<<endl;

        //checking to see tail is initialized

        if(tail_== NULL)
        {
            std::cout<<"The tail is NULL, should initialize only once"<<endl;
            tail_=newDNode; //
            tail_->Setprev(head_);//
            tail_->Setnext(NULL); //this is equivalent to newDNode->Setnext(NULL)

            newDNode->Setindex(1);
            size_ = 1;
        }
    }
}


template <typename T>
void DoublyLinkedList<T>::Addafter (T value)
{
    //add to head
    DNode<T> * newDNode = new DNode<T>(value);

    newDNode->Setnext(NULL); //equivalent to tail_->Setnext(NULL)

    if(tail_!= NULL)
    {
        std::cout<<"checkpoint"<<std::endl;
        tail_->Setnext(newDNode);//establish link to the new tail
        newDNode->Setprev(tail_);//getting address of old_tail
        int tmpidx = tail_->Getindex();
        tmpidx++;
        tail_=newDNode; //swap
        tail_->Setindex(tmpidx);
        size_++;
    }

    else
    {
        // last node in the list
        tail_=newDNode;
    std::cout<<"tail initialized"<<std::endl;
        //checking if head is initialized
       if(head_ == NULL)
        {
            std::cout<<"head gets the same node"<<std::endl;
            head_=newDNode; //
            head_->Setnext(tail_);//
            head_->Setprev(NULL); //this is equivalent to newDNode->Setnext(NULL)

            newDNode->Setindex(1);
            size_ = 1;
        }
    }


}



template <typename T>
void DoublyLinkedList<T>::Insert(T value,int index)
{
    if(index > size_)
    {
        std::cout<<"index out of bound, no insertion is performed"<<std::endl;
    }
    else
    {
    DNode<T> * newDNode = new DNode<T>(value);

    DNode<T>* tmp = head_;

        if(tmp != NULL)
        {
            while(tmp->Getnext() != NULL && tmp->Getindex()< index-1)
            {
                 tmp = tmp->Getnext();
                 std::cout<<"loop check pt"<<std::endl;
            }
            //swap out pointer
            newDNode->Setnext(tmp->Getnext()); //inserted DNode gets the previous address of next DNode
            newDNode->Setprev(tmp);
            newDNode->Setindex(index); //obtaining the index
            tmp->Setnext(newDNode); //the previous DNode gets the address of inserted DNode
            tmp = newDNode->Getnext(); //jump to newDNode's address
            tmp->Setprev(newDNode);
                        //continue the loop
            while(tmp->Getnext() != NULL)
            {
                tmp = tmp->Getnext();
                int tmpidx = tmp->Getindex();
                tmp->Setindex(tmpidx++); //reindex all the remaining DNode
            }

          size_++;

        }
    }
}
template <typename T>
T  DoublyLinkedList<T>::Get(int index)
{
    if(index > size_ || index <= 0)
    {
        std::cout<<"index out of bound, can't get data"<<std::endl;
    }
    else
    {
        DNode<T> * tmp = head_;

       while(tmp->Getindex() != index)
        {
            tmp = tmp->Getnext();
        };

        return  tmp->Getdatum();
    }

}

template <typename T>
T DoublyLinkedList<T>::Remove(int index)
{
    if(index > size_)
    {
        std::cout<<"index out of bound, nothing to remove"<<std::endl;
    }
    else
    {
        DNode<T>* tmp = head_;
        DNode<T> * prev_tmp;

        if(tmp != NULL)
        {
            //stop before the index DNode
            while(tmp->Getnext() != NULL || tmp.Getindex()< index-1)
            {
                 tmp = tmp->Getnext();
            }
            //swap out pointer
            prev_tmp = tmp; //gets the address of preceding DNode
            tmp = tmp->Getnext(); //moves to the DNode that's getting removed
            tmp = tmp->Getnext(); //moves over the deleting node;
            prev_tmp->Setnext(tmp); //gets the address of the next DNode
            tmp->Setprev(prev_tmp);//gets the address of preceding node;
            tmp = tmp->Getprev();
            delete tmp; //jump to newDNode's address

            //continue the loop using the prev pointer
            while(prev_tmp->Getnext() != NULL)
            {
                prev_tmp = prev_tmp->Getnext();
                prev_tmp->Setindex(prev_tmp->Getindex()--);//reindexing
            }

          size_--;

        }
    }

}

template <typename T>
int DoublyLinkedList<T>::IndexOf(T value)
{
    DNode<T>* tmp = head_;

    do
    {
      if(tmp->Getdatum()== value)
        return tmp->Getindex();

      tmp=tmp->Getnext();

    }while(tmp != NULL);
}

template <typename T>
int DoublyLinkedList<T>::Size()
{
    return size_;
}

template <typename T>
ListIterator<T> DoublyLinkedList<T>::Iterator()
{
    ListIterator<T> current_DNode(head_);

    return current_DNode;
}

