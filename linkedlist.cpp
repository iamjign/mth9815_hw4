#include "linkedlist.h"



template <typename T>
LinkedList<T>::~LinkedList()
{

    while(head_!= NULL )
    {
        Node<T>* tmp1 = head_;
        head_=tmp1->Getnext();
        delete tmp1;

    };
    delete head_;//dtor
}

template <typename T>
LinkedList<T>::LinkedList(const LinkedList<T>& other)
{

    //copy ctor
}

template <typename T>
void LinkedList<T>::Add(T value)
{

    Node<T> * newNode = new Node<T>(value);
    newNode->Setnext(NULL);

     // Create a temp pointer
    Node<T> *tmp = head_;

    if ( tmp != NULL )
    {// Nodes already present in the list
    // Parse to end of list
    while ( tmp->Getnext() != NULL )
        {
        tmp = tmp->Getnext();
        }

    // Point the last node to the new node
    tmp->Setnext(newNode);
    int tmpidx=tmp->Getindex()+1;
    newNode->Setindex(tmpidx);
    size_ ++;
    }
    else
    {
    // First node in the list
    head_ = newNode;
    head_->Setindex(1);
    size_ = 1;
    }
}

template <typename T>
void LinkedList<T>::Insert(T value,int index)
{
    if(index > size_)
    {
        std::cout<<"index out of bound, no insertion is performed"<<std::endl;
    }
    else
    {
    Node<T> * newNode = new Node<T>(value);

    Node<T> tmp = head_;

        if(tmp != NULL)
        {
            while(tmp->Getnext() != NULL && tmp->Getindex()< index-1)
            {
                 tmp = tmp->Getnext();
            }
            //swap out pointer
            newNode->Setnext(tmp->Getnext()); //inserted node gets the address of next node
            newNode->Setindex(index); //obtaining the index
            tmp->Setnext(newNode); //the previous node gets the address of inserted node
            tmp = tmp->Getnext(); //jump to newNode's address
            //continue the loop
            while(tmp->Getnext() != NULL)
            {
                tmp = tmp->Getnext();
                tmp->Setindex(tmp->Getindex()++); //reindex all the remaining node
            }

          size_++;

        }
    }
}
template <typename T>
T  LinkedList<T>::Get(int index)
{
    if(index > size_)
    {
        std::cout<<"index out of bound, can't get data"<<std::endl;
    }
    else
    {
        Node<T> * tmp = head_;

        while(tmp->Getindex()<index)
        {
            tmp = tmp->Getnext();
        }

        return tmp->Getdatum();
    }

}

template <typename T>
T LinkedList<T>::Remove(int index)
{
    if(index > size_)
    {
        std::cout<<"index out of bound, nothing to remove"<<std::endl;
    }
    else
    {
        Node<T>* tmp = head_;
        Node<T> *prev;

        if(tmp != NULL)
        {
            //stop before the index node
            while(tmp->Getnext() != NULL || tmp.Getindex()< index-1)
            {
                 tmp = tmp->Getnext();
            }
            //swap out pointer
            prev = tmp; //gets the address of previous node
            tmp = tmp->Getnext(); //moves to the node that's getting removed
            prev->Setnext(tmp->Getnext()); //gets the address of the next node
            delete tmp; //jump to newNode's address

            //continue the loop using the prev pointer
            while(prev->Getnext() != NULL)
            {
                prev = prev->Getnext();
                prev->Setindex(prev->Getindex()--);//reindexing
            }

          size_--;

        }
    }
}

template <typename T>
int LinkedList<T>::IndexOf(T value)
{
    Node<T>* tmp = head_;

    do
    {
      if(tmp->Getdatum()== value)
        return tmp->Getindex();

      tmp=tmp->Getnext();

    }while(tmp != NULL);
}

template <typename T>
int LinkedList<T>::Size()
{
    return size_;
}

template <typename T>
ListIterator<T> LinkedList<T>::Iterator()
{
    ListIterator<T> current_node(head_);

    return current_node;
}
