#ifndef LinkedList_H
#define LinkedList_H

#include "node.h"
#include <iostream>

using namespace std;

template<class T>
class  LinkedList {
    public:
        LinkedList():head_(NULL) {};
        LinkedList(const LinkedList<T>& other);
        ~LinkedList();

        void Add(T value);
        void Insert(T value,int index);
        T  Get(int index);
        T Remove(int index);
        int IndexOf(T value);
        int Size();
        ListIterator<T> Iterator();

    private:
        Node<T>* head_;

        int size_;

};
#endif // LinkedList_H
