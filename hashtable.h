#ifndef HASHTABLE_H
#define HASHTABLE_H

#include <iostream>
#include <vector>
#include <string>

using namespace std;

template<typename K, typename V>
class Bucket
{
Bucket(const K &k, const V &v) : val(v), key(k) {}
};

template<typename T>
class Hasher
{
virtual static size_t hashfunction(const T& value) = 0;
};

template<typename T>
class Equalitypredicate
{

virtual bool Isequal(const T& value) = 0;

}

template<> class Hasher<string>
{
  static size_t hashFunction(const string& s)
  {
    return hash<string>()(s);
  }
};

//template<> class Hasher<int> {
//  static size_t hashFunction(const int m) {
//    return hash<int>()(m);
//  }
//};

template<typename K, typename V, typename HashGenerator = Hasher<K> >
class HashTable {
public:
  vector<vector<Bucket<K, V> > > table;
  HashTable(int size) {
    for(int i = 0; i < size; i++) {
      vector<Bucket<K, V> > v;
      table.push_back(v);
    }
  }
  ~HashTable() {}
  void set(const K &k, const V &v) {
    Bucket<K, V> b(k, v);
    for(int i = 0; i < table[hash(k)].size(); i++)
      if(table[hash(k)][i].key == k) {
        table[hash(k)][i] = b;
        return;
      }
    table[hash(k)].push_back(b);
  }
  V get(const K &k) {
    for(int i = 0; i < table[hash(k)].size(); i++)
      if(table[hash(k)][i].key == k)
        return table[hash(k)][i].val;
  }
  bool exist(const K &k) {
    for(int i = 0; i < table[hash(k)].size(); i++)
      if(table[hash(k)][i].key == k)
        return true;
    return false;
  }
  size_t hash(const K &k) {
    return HashGenerator::hashFunction(k) % table.size();
  }
};









#endif
