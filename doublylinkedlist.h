#ifndef DOUBLYLINKEDLIST_H
#define DOUBLYLINKEDLIST_H

#include "linkedlist.h"
#include "dnode.h"

template <typename T>
class DoublyLinkedList : public LinkedList<T>
{
    public:
        DoublyLinkedList(); //{head_->Setnext(NULL); head_->Setprev(NULL);};

        virtual ~DoublyLinkedList();
        DoublyLinkedList(const DoublyLinkedList& other);

        void Addbefore(T value);
        void Addafter(T value);
        void Insert(T value,int index);
        T  Get(int index);
        T Remove(int index);
        int IndexOf(T value);
        int Size();
        ListIterator<T> Iterator();


    private:
        DNode<T>* head_;
        DNode<T>* tail_;
        int size_ = 55;
};

#endif // DOUBLYLINKEDLIST_H
