#include "node.h"



template<typename T>
Node<T>::Node()
{
    //ctor
}

template<class T>
Node<T>::Node(T value): datum_(value)
{

}

template<class T>
Node<T>::~Node()
{
    //dtor
}

template<class T>
Node<T>::Node(const Node<T>& other)
{
  this->Setdatum(other->Getdatum());
  this->Setindex(other->Getindex());  //copy ctor;
  this->Setnext(other->Getnext());
}



template<class T>
ListIterator<T>::ListIterator(Node<T>* head_node)
{
    current_node = head_node;
}


template<class T>
T ListIterator<T>::Next()
{
    Node<T>* tmp_node;
    tmp_node = current_node->Getnext();
    return tmp_node->Getdatum();
}

template<class T>
bool ListIterator<T>::hasNext()
{
    if(current_node->Getnext()==0)
        return false;
}
